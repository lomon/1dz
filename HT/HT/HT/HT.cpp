// HT.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <cstdlib>

#define PRIME_SIZE 29 
using namespace std;

class Person 
{
public:
	Person *next; // ��� ������������� �������� �������� ����� �������� � ����������� ������.
	string name;
	string surname;
	int age;

	Person()
	{
		this->next = NULL;
	}

	Person(string name, string surname, int age = 0)
	{
		this->name = name;
		this->surname = surname;
		this->age = age;
		this->next = NULL;
	}

	~Person()
	{
		//cout << "Delete " << this->name << endl;
		if (this->next != NULL)
		{
			delete this->next;
		}
	}
};

class HashTable // ���-�������, �������������� � ���� ������� ��������� (������� � ���� ������� ������������ ������).
{
	Person *table[PRIME_SIZE];

	// ���-������� ������� ����� ASCII �����, ����� �� ��������� �
	// �������� ������� �� �������.
	static int hash(string str)
	{
		int asciisum = 0;
		for (int i = 0; i < str.length(); i++)
		{
			asciisum += str[i];
		}
		return asciisum % PRIME_SIZE;
	}

public:

	HashTable()
	{
		for (int i = 0; i < PRIME_SIZE; i++)
		{
			table[i] = NULL;
		}
	}

	~HashTable()
	{
		cout << "Delete table\n";
		for (int i = 0; i < PRIME_SIZE; i++)
		{
			delete table[i];
		}
	}

	// ��������� ������� � �������
	void push(string name, string surname, int age)
	{
		int hashNumber = hash(name);
		Person *pers = new Person(name, surname, age);
		Person *place = table[hashNumber];
		if (place == NULL)
		{
			table[hashNumber] = pers;
			return;
		}

		while (place->next != NULL)
		{
			place = place->next;
		}
		place->next = pers;
	}

	// �������� ������� �� ������� �� ��� �����.
	Person* find(string name)
	{
		int hashNumber = hash(name);
		Person *result = table[hashNumber];
		if (!result)
		{
			cout << "Element not found" << endl;
			return NULL;
		}
		while (result->name != name)
		{
			if (!result->next)
			{
				cout << "Element not found" << endl;
				break;
			}
			result = result->next;
		}
		return result;
	}
};

int main()
{
	HashTable newTable;
	newTable.push("Artyom", "Devyatov", 20);
	newTable.push("Vasya", "Petrov", 23);
	newTable.push("Ilja", "Saveljev", 28);
	newTable.push("Ilaj", "Savanna", 43); // ����� �������� � Ilja
	newTable.push("Dmitry", "Kuzychev", 31);

	Person * search = newTable.find("Ilja");
	if (search)
	{
		cout << search->surname << endl;
	}
	system("pause");
	return 0;
}